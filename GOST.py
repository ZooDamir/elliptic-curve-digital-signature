import secrets  # для криптографически надёжной генерации псевдослучайных чисел
import random  # для генерации простых числе (не криптографически надёжно)
import subprocess  # для запуска внешней программы (хеша)
import os  # для работы с ОС
import math  # математические функции
import multiprocessing  # для распаралелливания


class EllipticCurve:
    """
    Класс для работы с эллиптическими кривыми
    """
    MAX_P_POINT_SEARCH_TIMES = 1000000

    class PPointException(Exception):
        """
        Вспомогательный класс для отслеживания ошибки при поиске точки на кривой
        """
        pass

    @staticmethod
    def _miller_rabin_(n, k=10):
        """
        Тест Миллера-Рабина на простоту
        :param n: число, которое необходимо проверить
        :param k: количество раундов (значение по умолчанию - 10)
        :return: True, если число простое, иначе False
        """
        if n == 2 or n == 3:
            return True
        if not n & 1:
            return False

        def check(a, s, d, n):
            x = pow(a, d, n)
            if x == 1:
                return True
            for _ in range(s - 1):
                if x == n - 1:
                    return True
                x = pow(x, 2, n)
            return x == n - 1

        s = 0
        d = n - 1

        while d % 2 == 0:
            d >>= 1
            s += 1

        for i in range(k):
            a = random.randrange(2, n - 1)
            if not check(a, s, d, n):
                return False
        return True

    def _generate_random_prime_(self, bit_count=512):
        """
        Метод для генерации простого числа указанной длины в битах
        :param bit_count: длина числа в битах
        :return: простое число
        """
        temp = secrets.randbits(bit_count)  # генерируем случайные биты, получаем их в виде числа
        k = int(math.log2(temp))  # вычисляем количество раундов для теста Миллера-Рабина
        while not self._miller_rabin_(temp, k):  # если наше число - не простое
            temp = secrets.randbits(bit_count)  # заново генерируем случайные биты
            k = int(math.log2(temp))  # пересчитываем необходимое количество раундов
        return temp  # возвращаем

    def multiply_point_by_number(self, point, number):
        """
        Метод для быстрого умножения точки на число
        Идея как при быстром возведении в степень по модулю
        :param point: точка, которую необходимо умножить
        :param number: число на которое необходимо умножить
        :return: точка, равная number * point
        """
        def get_bits_of_number(num):
            """
            Генератор для получения битов числа
            :param num: число, биты которого необходимо получить
            :return:
            """
            while num:  # пока число не равно 0
                yield num & 1  # "возвращаем" последний бит (0 или 1)
                num >>= 1  # сдвигаем число на 1 бит вправо

        helper = Point(point.x, point.y)  # создаём точку с такими же координатами, чтобы ничего не испортить
        result = Point(0, 0)  # точка, которая получится в результате умножения, изначально - (0, 0)
        for bit in get_bits_of_number(number):  # проходимся по всем битам
            if bit == 1:  # если очередной бит равен 1
                result = self.add_points(result, helper)  # добавляем значение helper к результату
            helper = self.add_points(helper, helper)  # удваиваем значение helper

        return result

    def add_points(self, first, second):
        """
        Метод для сложения точек на кривой
        :param first: первая точка
        :param second: вторая точка
        :return: точку, равную first + second
        """
        # если складываем с 0, то получаем - вторую точку
        if first == Point(0, 0):
            return second
        if second == Point(0, 0):
            return first

        if first == second:
            temp1 = (3 * first.x ** 2 + self.a) % self.p
            temp2 = pow((2 * first.y), (self.p - 2), self.p)
            lambd = (temp1 * temp2) % self.p
        else:
            temp1 = (second.y - first.y) % self.p
            temp2 = pow((second.x - first.x), (self.p - 2), self.p)
            lambd = (temp1 * temp2) % self.p

        # если у нас знаменатель - не нулевой, то вычисляем координаты, иначе - точка (0, 0)
        if temp2 != 0:
            x3 = (lambd ** 2 - first.x - second.x) % self.p
            y3 = (lambd * (first.x - x3) - first.y) % self.p
            return Point(x3, y3)
        return Point(0, 0)

    def equation(self, point):
        """
        Метод для проверки удовалетворяет ли точка уравнению кривой
        :param point: точка, которую необходимо проверить
        :return: True, если точка удовалетворяет уравнению, иначе False
        """
        return pow(point.y, 2, self.p) == (pow(point.x, 3) + self.a * point.x + self.b) % self.p

    def equation_as_function(self, x):
        """
        Метод для получения занчения y^2 для заданного x
        :param x: число, для которого необходимо узнать y^2
        :return: y^2
        """
        return (x ** 3 + self.a * x + self.b) % self.p

    def invariant(self):
        """
        Инвариант заданной кривой
        :return: значение инварианта
        """
        temp1 = (4 * self.a ** 3) % self.p
        temp2 = pow((4 * self.a ** 3 + 27 * self.b ** 2), self.p - 2, self.p)
        return (1728 * temp1 * temp2) % self.p

    def _count_points_with_given_x_(self, x):
        """
        Метод для подсчёта точек на кривой для фиксированного х
        :param x: фиксированная координата х
        :return: количество таких точек
        """
        ys = range(self.p)  # список координат у
        counter = 0  # счётчик точек
        for y in ys:  # проходим по всем у
            if self.equation(Point(x, y)):  # если точка лежит на кривой
                counter += 1  # увеличиваем счётчик на 1
            if counter == 2:
                return counter
        return counter  # возвращаем результат подсчёта

    def _naive_algorithm_(self):
        """
        Простейший алгоритм для подсчёта точек на кривой
        Проверяет точки парралельно в несколько процессов (задействует все ядра компьютера)
        :return: количество точек на кривой
        """
        print(f'Считаем точки на кривой с p = {self.p}')  # выводим с каким p имеем дело, чтобы могли оцениить сколько будет выполняться работа
        xs = [x for x in range(self.p)]  # список координат х

        counter = 1  # точка (0, 0)
        with multiprocessing.Pool() as pool:  # выделяем пул воркеров (1 вокрер на каждое процессорное ядеро)
            data = pool.map(self._count_points_with_given_x_, xs)  # этому пулу выдаём задачу выполнить функцию _count_points_with_given_x_ для всех x из xs
            counter += sum(data)  # к счётчику добавляем сумму того, что вернёт нам результат обработки всех точек т.е. у нас в результате в data хрнится массив из 0, 1 и 2

        return counter

    def get_m(self):
        """
        Метод для получения точек кривой
        :return:
        """
        m = 0
        if len(str(self.p)) <= 7:
            # если достаточно маленький модуль, то считаем "в лоб"
            m = self._naive_algorithm_()
        elif len(str(self.p)) <= 30:
            # тут мог быть алгоритм маленьких-больших шагов
            pass
        else:
            # сюда мы не должны никогда попасть т.к. инчае будет считаться очень долго
            raise ValueError()
        return m

    @staticmethod
    def _get_prime_factors_(number):
        """
        Метод для разложения числа на простые множители
        :param number: число, которое необходимо разложить
        :return: список простых делителей
        """
        output = set()  # инициализируем пустое множество

        primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101,
                  103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199,
                  211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317,
                  331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443,
                  449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577,
                  587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701,
                  709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839,
                  853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983,
                  991, 997, 1009, 1013, 1019, 1021, 1031, 1033, 1039, 1049, 1051, 1061, 1063, 1069, 1087, 1091, 1093,
                  1097, 1103, 1109, 1117, 1123, 1129, 1151, 1153, 1163, 1171, 1181, 1187, 1193, 1201, 1213, 1217, 1223,
                  1229, 1231, 1237, 1249, 1259, 1277, 1279, 1283, 1289, 1291, 1297, 1301, 1303, 1307, 1319, 1321, 1327,
                  1361, 1367, 1373, 1381, 1399, 1409, 1423, 1427, 1429, 1433, 1439, 1447, 1451, 1453, 1459, 1471, 1481,
                  1483, 1487, 1489, 1493, 1499, 1511, 1523, 1531, 1543, 1549, 1553, 1559, 1567, 1571, 1579, 1583, 1597,
                  1601, 1607, 1609, 1613, 1619, 1621, 1627, 1637, 1657, 1663, 1667, 1669, 1693, 1697, 1699, 1709, 1721,
                  1723, 1733, 1741, 1747, 1753, 1759, 1777, 1783, 1787, 1789, 1801, 1811, 1823, 1831, 1847, 1861, 1867,
                  1871, 1873, 1877, 1879, 1889, 1901, 1907, 1913, 1931, 1933, 1949, 1951, 1973, 1979, 1987, 1993, 1997,
                  1999, 2003, 2011, 2017, 2027, 2029, 2039, 2053, 2063, 2069, 2081, 2083, 2087, 2089, 2099, 2111, 2113,
                  2129, 2131, 2137, 2141, 2143, 2153, 2161, 2179, 2203, 2207, 2213, 2221, 2237, 2239, 2243, 2251, 2267,
                  2269, 2273, 2281, 2287, 2293, 2297, 2309, 2311, 2333, 2339, 2341, 2347, 2351, 2357, 2371, 2377, 2381,
                  2383, 2389, 2393, 2399, 2411, 2417, 2423, 2437, 2441, 2447, 2459, 2467, 2473, 2477, 2503, 2521, 2531,
                  2539, 2543, 2549, 2551, 2557, 2579, 2591, 2593, 2609, 2617, 2621, 2633, 2647, 2657, 2659, 2663, 2671,
                  2677, 2683, 2687, 2689, 2693, 2699, 2707, 2711, 2713, 2719, 2729, 2731, 2741, 2749, 2753, 2767, 2777,
                  2789, 2791, 2797, 2801, 2803, 2819, 2833, 2837, 2843, 2851, 2857, 2861, 2879, 2887, 2897, 2903, 2909,
                  2917, 2927, 2939, 2953, 2957, 2963, 2969, 2971, 2999, 3001, 3011, 3019, 3023, 3037, 3041, 3049, 3061,
                  3067, 3079, 3083, 3089, 3109, 3119, 3121, 3137, 3163, 3167, 3169, 3181, 3187, 3191, 3203, 3209, 3217,
                  3221, 3229, 3251, 3253, 3257, 3259, 3271, 3299, 3301, 3307, 3313, 3319, 3323, 3329, 3331, 3343, 3347,
                  3359, 3361, 3371, 3373, 3389, 3391, 3407, 3413, 3433, 3449, 3457, 3461, 3463, 3467, 3469, 3491, 3499,
                  3511, 3517, 3527, 3529, 3533, 3539, 3541, 3547, 3557, 3559, 3571, 3581, 3583, 3593, 3607, 3613, 3617,
                  3623, 3631, 3637, 3643, 3659, 3671, 3673, 3677, 3691, 3697, 3701, 3709, 3719, 3727, 3733, 3739, 3761,
                  3767, 3769, 3779, 3793, 3797, 3803, 3821, 3823, 3833, 3847, 3851, 3853, 3863, 3877, 3881, 3889, 3907,
                  3911, 3917, 3919, 3923, 3929, 3931, 3943, 3947, 3967, 3989, 4001, 4003, 4007, 4013, 4019, 4021, 4027,
                  4049, 4051, 4057, 4073, 4079, 4091, 4093, 4099, 4111, 4127, 4129, 4133, 4139, 4153, 4157, 4159, 4177,
                  4201, 4211, 4217, 4219, 4229, 4231, 4241, 4243, 4253, 4259, 4261, 4271, 4273, 4283, 4289, 4297, 4327,
                  4337, 4339, 4349, 4357, 4363, 4373, 4391, 4397, 4409, 4421, 4423, 4441, 4447, 4451, 4457, 4463, 4481,
                  4483, 4493, 4507, 4513, 4517, 4519, 4523, 4547, 4549, 4561, 4567, 4583, 4591, 4597, 4603, 4621, 4637,
                  4639, 4643, 4649, 4651, 4657, 4663, 4673, 4679, 4691, 4703, 4721, 4723, 4729, 4733, 4751, 4759, 4783,
                  4787, 4789, 4793, 4799, 4801, 4813, 4817, 4831, 4861, 4871, 4877, 4889, 4903, 4909, 4919, 4931, 4933,
                  4937, 4943, 4951, 4957, 4967, 4969, 4973, 4987, 4993, 4999, 5003, 5009, 5011, 5021, 5023, 5039, 5051,
                  5059, 5077, 5081, 5087, 5099, 5101, 5107, 5113, 5119, 5147, 5153, 5167, 5171, 5179, 5189, 5197, 5209,
                  5227, 5231, 5233, 5237, 5261, 5273, 5279, 5281, 5297, 5303, 5309, 5323, 5333, 5347, 5351, 5381, 5387,
                  5393, 5399, 5407, 5413, 5417, 5419, 5431, 5437, 5441, 5443, 5449, 5471, 5477, 5479, 5483, 5501, 5503,
                  5507, 5519, 5521, 5527, 5531, 5557, 5563, 5569, 5573, 5581, 5591, 5623, 5639, 5641, 5647, 5651, 5653,
                  5657, 5659, 5669, 5683, 5689, 5693, 5701, 5711, 5717, 5737, 5741, 5743, 5749, 5779, 5783, 5791, 5801,
                  5807, 5813, 5821, 5827, 5839, 5843, 5849, 5851, 5857, 5861, 5867, 5869, 5879, 5881, 5897, 5903, 5923,
                  5927, 5939, 5953, 5981, 5987, 6007, 6011, 6029, 6037, 6043, 6047, 6053, 6067, 6073, 6079, 6089, 6091,
                  6101, 6113, 6121, 6131, 6133, 6143, 6151, 6163, 6173, 6197, 6199, 6203, 6211, 6217, 6221, 6229, 6247,
                  6257, 6263, 6269, 6271, 6277, 6287, 6299, 6301, 6311, 6317, 6323, 6329, 6337, 6343, 6353, 6359, 6361,
                  6367, 6373, 6379, 6389, 6397, 6421, 6427, 6449, 6451, 6469, 6473, 6481, 6491, 6521, 6529, 6547, 6551,
                  6553, 6563, 6569, 6571, 6577, 6581, 6599, 6607, 6619, 6637, 6653, 6659, 6661, 6673, 6679, 6689, 6691,
                  6701, 6703, 6709, 6719, 6733, 6737, 6761, 6763, 6779, 6781, 6791, 6793, 6803, 6823, 6827, 6829, 6833,
                  6841, 6857, 6863, 6869, 6871, 6883, 6899, 6907, 6911, 6917, 6947, 6949, 6959, 6961, 6967, 6971, 6977,
                  6983, 6991, 6997, 7001, 7013, 7019, 7027, 7039, 7043, 7057, 7069, 7079, 7103, 7109, 7121, 7127, 7129,
                  7151, 7159, 7177, 7187, 7193, 7207, 7211, 7213, 7219, 7229, 7237, 7243, 7247, 7253, 7283, 7297, 7307,
                  7309, 7321, 7331, 7333, 7349, 7351, 7369, 7393, 7411, 7417, 7433, 7451, 7457, 7459, 7477, 7481, 7487,
                  7489, 7499, 7507, 7517, 7523, 7529, 7537, 7541, 7547, 7549, 7559, 7561, 7573, 7577, 7583, 7589, 7591,
                  7603, 7607, 7621, 7639, 7643, 7649, 7669, 7673, 7681, 7687, 7691, 7699, 7703, 7717, 7723, 7727, 7741,
                  7753, 7757, 7759, 7789, 7793, 7817, 7823, 7829, 7841, 7853, 7867, 7873, 7877, 7879, 7883, 7901, 7907,
                  7919]  # первая 1000 простых

        # поскольку число - относительно небольшое его простые делители будут тоже небольшими
        # значит, мы можем достаточно быстро разложить число просто деля его на простые
        i = 0
        while number != 1:  # пока не получили 1
            if number % primes[i] == 0:  # проверяем делит ли очередное простое наше число нацело
                number = number // primes[i]  # есди да, то делим
                output.add(primes[i])  # в множесто делителей заносим простое
            else:  # если нет
                i += 1  # то берём следующие простое

        return output

    def _get_small_q_(self):
        """
        Метод для получения q при малом значении p
        :return: q такое, что m % q = 0 и q - простое
        """
        if self._miller_rabin_(self.m, math.isqrt(self.m)):  # если m - простое, то возвращаем его
            return self.m

        q = max(self._get_prime_factors_(self.m))  # иначе берём максимум из делителей

        return q

    def __init__(self, a, b, length):
        """
        Конструктор, инициализирует поля
        Берёт кривые из RFC 5639
        При размерности length <= 20, генерирует случайную кривую
        :param a: коэффициент а
        :param b: коэффициент b
        :param length: длина модуля в битах
        """
        # в RFC для всех длин дано q и значение кофактора h для группы порядка q
        # поскольку h = 1 для всех кривых, в них везде m = q
        if length == 160:
            self.p = int('E95E4A5F737059DC60DFC7AD95B3D8139515620F', 16)
            self.a = int('340E7BE2A280EB74E2BE61BADA745D97E8F7C300', 16)
            self.b = int('1E589A8595423412134FAA2DBDEC95C8D8675E58', 16)
            self.q = int('E95E4A5F737059DC60DF5991D45029409E60FC09', 16)
            self.m = self.q
        elif length == 192:
            self.p = int('C302F41D932A36CDA7A3463093D18DB78FCE476DE1A86297', 16)
            self.a = int('6A91174076B1E0E19C39C031FE8685C1CAE040E5C69A28EF', 16)
            self.b = int('469A28EF7C28CCA3DC721D044F4496BCCA7EF4146FBF25C9', 16)
            self.q = int('C302F41D932A36CDA7A3462F9E9E916B5BE8F1029AC4ACC1', 16)
            self.m = self.q
        elif length == 224:
            self.p = int('D7C134AA264366862A18302575D1D787B09F075797DA89F57EC8C0FF', 16)
            self.a = int('68A5E62CA9CE6C1C299803A6C1530B514E182AD8B0042A59CAD29F43', 16)
            self.b = int('2580F63CCFE44138870713B1A92369E33E2135D266DBB372386C400B', 16)
            self.q = int('D7C134AA264366862A18302575D0FB98D116BC4B6DDEBCA3A5A7939F', 16)
            self.m = self.q
        elif length == 256:
            self.p = int('A9FB57DBA1EEA9BC3E660A909D838D726E3BF623D52620282013481D1F6E5377', 16)
            self.a = int('7D5A0975FC2C3057EEF67530417AFFE7FB8055C126DC5C6CE94A4B44F330B5D9', 16)
            self.b = int('26DC5C6CE94A4B44F330B5D9BBD77CBF958416295CF7E1CE6BCCDC18FF8C07B6', 16)
            self.q = int('A9FB57DBA1EEA9BC3E660A909D838D718C397AA3B561A6F7901E0E82974856A7', 16)
            self.m = self.q
        elif length == 320:
            self.p = int('D35E472036BC4FB7E13C785ED201E065F98FCFA6F6F40DEF4F92B9EC7893EC28FCD412B1F1B32E27', 16)
            self.a = int('3EE30B568FBAB0F883CCEBD46D3F3BB8A2A73513F5EB79DA66190EB085FFA9F492F375A97D860EB4', 16)
            self.b = int('520883949DFDBC42D3AD198640688A6FE13F41349554B49ACC31DCCD884539816F5EB4AC8FB1F1A6', 16)
            self.q = int('D35E472036BC4FB7E13C785ED201E065F98FCFA5B68F12A32D482EC7EE8658E98691555B44C59311', 16)
            self.m = self.q
        elif length == 384:
            self.p = int('8CB91E82A3386D280F5D6F7E50E641DF152F7109ED5456B412B1DA197FB71123ACD3A729901D1A71874700133107EC53', 16)
            self.a = int('7BC382C63D8C150C3C72080ACE05AFA0C2BEA28E4FB22787139165EFBA91F90F8AA5814A503AD4EB04A8C7DD22CE2826', 16)
            self.b = int('04A8C7DD22CE28268B39B55416F0447C2FB77DE107DCD2A62E880EA53EEB62D57CB4390295DBC9943AB78696FA504C11', 16)
            self.q = int('8CB91E82A3386D280F5D6F7E50E641DF152F7109ED5456B31F166E6CAC0425A7CF3AB6AF6B7FC3103B883202E9046565', 16)
            self.m = self.q
        elif length == 512:
            self.p = int('AADD9DB8DBE9C48B3FD4E6AE33C9FC07CB308DB3B3C9D20ED6639CCA703308717D4D9B009BC66842AECDA12AE6A380E62881FF2F2D82C68528AA6056583A48F3', 16)
            self.a = int('7830A3318B603B89E2327145AC234CC594CBDD8D3DF91610A83441CAEA9863BC2DED5D5AA8253AA10A2EF1C98B9AC8B57F1117A72BF2C7B9E7C1AC4D77FC94CA', 16)
            self.b = int('3DF91610A83441CAEA9863BC2DED5D5AA8253AA10A2EF1C98B9AC8B57F1117A72BF2C7B9E7C1AC4D77FC94CADC083E67984050B75EBAE5DD2809BD638016F723', 16)
            self.q = int('AADD9DB8DBE9C48B3FD4E6AE33C9FC07CB308DB3B3C9D20ED6639CCA70330870553E5C414CA92619418661197FAC10471DB1D381085DDADDB58796829CA90069', 16)
            self.m = self.q
        elif length <= 20:
            self.p = self._generate_random_prime_(length)  # модуль кривой
            self.a = a  # коэффициент a
            self.b = b  # коэффициент b
            self.m = self.get_m()  # порядок группы точек кривой
            self.q = self._get_small_q_()  # порядок циклической подгрупп
        else:
            raise ValueError()  # в случае относительно большой кривой (не стандартного размера), бросаем ошибку
        self.length = length

    def __repr__(self):
        """
        Метод для вывода уравнения кривой при выводе её на консоль (при вызове print(<кривая>))
        :return:
        """
        return f'y^2 = x^3 + {self.a}x + {self.b} (mod {self.p})'

    @staticmethod
    def get_control_curve(length):
        """
        Метод получения кривой, указанной в ГОСТе
        :param length: длина в битах для p
        :return: контрольную кривую
        """
        if length == 256:
            # записываем параметры кривой
            a = 7
            b = 43308876546767276905765904595650931995942111794451039583252968842033849580414
            p = 57896044618658097711785492504343953926634992332820282019728792003956564821041
            m = 57896044618658097711785492504343953927082934583725450622380973592137631069619
            q = 57896044618658097711785492504343953927082934583725450622380973592137631069619
            # берём кривую из стандартов, чтобы не генерировать динамически (так быстрее)
            curve = EllipticCurve(a, b, 160)
            curve.a = a
            curve.b = b
            curve.p = p
            curve.m = m
            curve.q = q
            curve.n = 1
            return curve
        else:
            # записываем параметры кривой
            a = 7
            b = 1518655069210828534508950034714043154928747527740206436194018823352809982443793732829756914785974674866041605397883677596626326413990136959047435811826396
            p = 3623986102229003635907788753683874306021320925534678605086546150450856166624002482588482022271496854025090823603058735163734263822371964987228582907372403
            m = 3623986102229003635907788753683874306021320925534678605086546150450856166623969164898305032863068499961404079437936585455865192212970734808812618120619743
            q = 3623986102229003635907788753683874306021320925534678605086546150450856166623969164898305032863068499961404079437936585455865192212970734808812618120619743
            # берём кривую из стандартов, чтобы не генерировать динамически (так быстрее)
            curve = EllipticCurve(a, b, 160)
            curve.a = a
            curve.b = b
            curve.p = p
            curve.m = m
            curve.q = q
            curve.n = 1
            return curve

    def get_point_with_q_rank(self):
        """
        Метод для получения точки P на кривой с порядком q
        :return:
        """
        t = None  # изначально - нет точки
        counter = 0
        while not t:  # пока не получили точку
            try:
                t = self._get_point_with_q_rank_()  # пытаемся получить
            except self.PPointException:  # если вылетело исключение
                t = None  # всё-ещё нет точки
                counter += 1  # увеличиваем число неудачных попыток на 1
                if counter > self.MAX_P_POINT_SEARCH_TIMES:  # если получили достаточно много плохих попыток
                    raise self.PPointException('Maximum times of search of P') # генерируем новую кривую (модуль => и порядок кривой), с такими же a и b
        return t

    def _get_point_with_q_rank_(self):
        """
        Метод для получения точки порядка q на кривой (может вызвать исключение)
        :return:
        """
        def generate_point_on_curve():
            """
            Метод для поиска случайной точки на кривой
            :return:
            """
            def legendre_sign(a, p):
                """
                Символ Лежандра
                :return:
                """
                temp = pow(a, (p - 1) // 2, p)

                if temp == p - 1:
                    return -1
                return temp  # используем формулу Эйлера для символа Лежандра с простым p

            def tonelli_shanks_algorithm(n, p):
                """
                Алгоритм Тоннели-Шенкса
                Позволяет найти x, такой что x^2 = n (mod p)
                :return:
                """
                def get_quadratic_non_residue(pr):
                    """
                    Метод для получения квадратного невычита по pr
                    :return:
                    """
                    while True:
                        candidate = secrets.randbits(pr.bit_length())  # генерируем случайное число
                        # число является квадратным невычитом тогда и только тогда, когда символ Лежанждра = -1 (mod q)
                        if legendre_sign(candidate, pr) == -1:
                            return candidate

                if legendre_sign(n, p) == -1:
                    raise self.PPointException('no integer solutions exist')

                if p % 4 == 3:  # тривиальный случай
                    return pow(n, (p + 1) // 4, p)

                # изначальные значени
                S = 0
                temp = p - 1

                # выделяем степени двойки
                while temp % 2 == 0:
                    S += 1
                    temp //= 2

                Q = temp

                z = get_quadratic_non_residue(p)  # получаем случайный квадратный невычет

                # вычисляем параметры алгоритма
                c = pow(z, Q, p)
                R = pow(n, (Q + 1) // 2, p)
                algo_t = pow(n, Q, p)
                M = S

                # пока не нашли такое число
                while True:
                    # если t % p = 1, то в R храниится ответ
                    if algo_t % p == 1:
                        return R

                    # находим наименьшее i такое, что algo_t ^ 2 ^ i = 1 (mod p)
                    t2 = pow(algo_t, 2, p)
                    for i in range(1, M):
                        if t2 % p == 1:
                            break
                        t2 = t2 * t2

                    # пересчитываем параметры
                    power = (M - i - 1)
                    b = pow(c, 2 ** power, p)
                    R = (R * b) % p
                    algo_t = (algo_t * b ** 2) % p
                    c = pow(b, 2, p)
                    M = i

            while True:
                x = secrets.randbelow(self.p)  # генерируем случайный х
                f = self.equation_as_function(x)  # вычисляем y^2

                # тривиальный случай, который подходит
                if f % self.p == 0:
                    break

                sign = legendre_sign(f, self.p)  # вычисляем символ Лежандра

                # если символ равен 1, то f - квадратный вычет
                if sign == 1:
                    break

            ans = tonelli_shanks_algorithm(f, self.p)  # вычисляем корень из f

            return Point(x, ans)  # возвращаем точку с получеными координатами

        # если длина - одна из стандартных, то возвращаем точку из стандарта
        # если длина достаточна мала (<= 20), то находим точку с ипользованием вычисления кофактора и случайной точки на кривой
        # в противном случае - ошибка
        if self.length == 160:
            x = int('BED5AF16EA3F6A4F62938C4631EB5AF7BDBCDBC3', 16)
            y = int('1667CB477A1A8EC338F94741669C976316DA6321', 16)
            point = Point(x, y)
        elif self.length == 192:
            x = int('C0A0647EAAB6A48753B033C56CB0F0900A2F5C4853375FD6', 16)
            y = int('14B690866ABD5BB88B5F4828C1490002E6773FA2FA299B8F', 16)
            point = Point(x, y)
        elif self.length == 224:
            x = int('0D9029AD2C7E5CF4340823B2A87DC68C9E4CE3174C1E6EFDEE12C07D', 16)
            y = int('58AA56F772C0726F24C6B89E4ECDAC24354B9E99CAA3F6D3761402CD', 16)
            point = Point(x, y)
        elif self.length == 256:
            x = int('8BD2AEB9CB7E57CB2C4B482FFC81B7AFB9DE27E1E3BD23C23A4453BD9ACE3262', 16)
            y = int('547EF835C3DAC4FD97F8461A14611DC9C27745132DED8E545C1D54C72F046997', 16)
            point = Point(x, y)
        elif self.length == 320:
            x = int('43BD7E9AFB53D8B85289BCC48EE5BFE6F20137D10A087EB6E7871E2A10A599C710AF8D0D39E20611', 16)
            y = int('14FDD05545EC1CC8AB4093247F77275E0743FFED117182EAA9C77877AAAC6AC7D35245D1692E8EE1', 16)
            point = Point(x, y)
        elif self.length == 384:
            x = int('1D1C64F068CF45FFA2A63A81B7C13F6B8847A3E77EF14FE3DB7FCAFE0CBD10E8E826E03436D646AAEF87B2E247D4AF1E', 16)
            y = int('8ABE1D7520F9C2A45CB1EB8E95CFD55262B70B29FEEC5864E19C054FF99129280E4646217791811142820341263C5315', 16)
            point = Point(x, y)
        elif self.length == 512:
            x = int('81AEE4BDD82ED9645A21322E9C4C6A9385ED9F70B5D916C1B43B62EEF4D0098EFF3B1F78E2D0D48D50D1687B93B97D5F7C6D5047406A5E688B352209BCB9F822', 16)
            y = int('7DDE385D566332ECC0EABFA9CF7822FDF209F70024A57B1AA000C55B881F8111B2DCDE494A5F485E5BCA4BD88A2763AED1CA2B2FA8F0540678CD1E0F3AD80892', 16)
            point = Point(x, y)
        elif self.length <= 20:
            point = generate_point_on_curve()  # генерируем точку на кривой
            zero = Point(0, 0)  # точка (0, 0)
            h = self.m // self.q  # вычисляем кофактор
            while self.multiply_point_by_number(point, self.q) != zero or point == zero:  # пока не нашли точку такого порядка или нашли (0, 0)
                point = generate_point_on_curve()  # генерируем точку на кривой

                point = self.multiply_point_by_number(point, h)  # умножаем её на кофактор
        else:
            raise ValueError()
        return point


class Point:
    """
    Класс для передставления точки
    """
    def __init__(self, x, y):
        """
        Конструктор
        :param x: координата х
        :param y: координата у
        """
        self.x = x
        self.y = y

    def __eq__(self, other):
        """
        Метод для сравнения двух точек через перегрузку оператора ==
        Сравнивает покоординатно
        :param other: точка, с которой необходимо сравнить
        :return:
        """
        return self.x == other.x and self.y == other.y

    def __repr__(self):
        """
        Метод для представления точки в виде строки (x, y)
        :return:
        """
        return f'({self.x}, {self.y})'


class GOST:
    """
    Класс для реализации ГОСТа
    """

    class DigitalSignature:
        """
        Класс электронной подписи
        """

        def __init__(self, r, s, length):
            """
            Конструктор
            :param r: r компонента подписи
            :param s: s компонента подписи
            :param length: длина p в битах (для контроля длин)
            """
            self.r = r
            self.s = s
            self.length = length

        def get_hex_signature(self):
            """
            Метод получения подписи в виде 16чной строки
            :return:
            """
            temp1 = hex(self.r).removeprefix('0x')  # преобразуем из числа в 16чную строку, убираем 0х
            temp2 = hex(self.s).removeprefix('0x')  # преобразуем из числа в 16чную строку, убираем 0х

            # полка длины меньше необходимых, дополняем 0ми
            while len(temp1) < self.length // 4:
                temp1 = '0' + temp1

            while len(temp2) < self.length // 4:
                temp2 = '0' + temp2

            return temp1 + temp2  # возвращаем конкатенацию строк

        @staticmethod
        def from_hex_signature(sign):
            """
            Метод для генерации объекта подписи из 16чной строки
            :param sign: 16чная строка, представляющая подпись
            :return:
            """

            if len(sign) % 2 != 0:
                raise ValueError('Неверная длина подписи')

            vec_length = len(sign) // 2  # считаем длину одной компонены

            r_vec = sign[0:vec_length]  # получаем r в виде 16чной строки
            s_vec = sign[vec_length:]  # получаем s в виде 16чной строки

            r = int(r_vec, 16)  # преобразуем из строки в число, с переводом из 16чной в 10ю
            s = int(s_vec, 16)  # преобразуем из строки в число, с переводом из 16чной в 10ю
            return GOST.DigitalSignature(r, s, vec_length // 4)  # возвращаем объект

        def __repr__(self):
            """
            Метод для представления подписи, как 16чной строки (перегрузка вывода на консоль)
            :return:
            """
            return self.get_hex_signature()

    LENGTH_256_CONTROL_S_VALUE = 574973400270084654178925310019147038455227042649098563933718999175515839552  # контрольное значение S в ГОСТе для длины 256 бит
    LENGTH_512_CONTROL_S_VALUE = 864523221707669519038849297382936917075023735848431579919598799313385180564748877195639672460179421760770893278030956807690115822709903853682831835159370  # контрольное значение S в ГОСТе для длины 512 бит

    def _check_modulus_p_(self, B):
        """
        Метод проверки p на соответсвие требований в ГОСТе:
        Для все i в [1..B] p^i != 1 (mod q)
        :param B: до скольки необходимо проверять
        :return:
        """
        for i in range(1, B + 1):  # прозодимся по всем i
            if pow(self.elliptic_curve.p, i, self.elliptic_curve.q) == 1:  # если равно 1, то возвращаем False
                return False
        return True  # если не вышли раньше - то True

    def _check_m_not_equal_p(self):
        """
        Метод проверки того, что m != p
        (Создан для сокращения записи)
        :return:
        """
        return self.elliptic_curve.m != self.elliptic_curve.p

    def _check_curve_invariant_(self):
        """
        Проверка соответсвие инварианта кривой требованиям ГОСТа
        :return:
        """
        temp = self.elliptic_curve.invariant()  # получаем инвариант
        return temp != 0 and temp != 1728

    def _check_conditions_(self):
        """
        Метод проверки соответсвия кривой требованиям ГОСТа
        :return:
        """
        if self.length == 256:
            return self._check_modulus_p_(31) and self._check_m_not_equal_p() and self._check_curve_invariant_()
        elif self.length == 512:
            return self._check_modulus_p_(131) and self._check_m_not_equal_p() and self._check_curve_invariant_()
        else:
            return self._check_modulus_p_(10) and self._check_m_not_equal_p() and self._check_curve_invariant_()

    def __init__(self, a, b, length, path_to_compiled_hash_function):
        """
        Конструктор
        :param a: коэффициент а кривой
        :param b: коэффициент b кривой
        :param length: длина модуля p в битах
        :param path_to_compiled_hash_function: путь к скомпилированному Стрибогу
        """
        self.length = length
        self.elliptic_curve = EllipticCurve(a, b, length)
        self.path_to_compiled_hash_function = path_to_compiled_hash_function

        while not self._check_conditions_():  # если кривая не соответсвует требованиям ГОСТа, то берётся новая
            self.elliptic_curve = EllipticCurve(a, b, length)

    def _get_hash_(self, M):
        """
        Метод для получения хеша сообщения
        Программа зеширования записывает в файл значение хеша, этот метод его считывает и удаляет файл
        :param M: сообщение, хеш которого необходимо получить
        :return:
        """
        temp_hash_code = subprocess.call(f'{self.path_to_compiled_hash_function} {self.length} {M}')  # значение, которое вернула программа
        with open(f'temp_hash{temp_hash_code}.txt') as f:  # файл в который она записала хеш формируется в виде temp_hash<код выхода>
            temp_hash = f.readline()  # считываем строку
            temp_hash = temp_hash.replace('-', '')  # убираем -
        os.remove(f'temp_hash{temp_hash_code}.txt')  # удаляем файл
        return temp_hash

    @staticmethod
    def _get_alpha_(hash_string):
        """
        Метод для получения альфы из хеша
        :param hash_string: хеш
        :return:
        """
        return int(hash_string, 16)

    def _get_e_from_hash_(self, hash_string):
        """
        Метод для получания e из хеша
        :param hash_string: хеш сообщения
        :return:
        """
        alpha = self._get_alpha_(hash_string)  # получаем альфу
        e = alpha % self.elliptic_curve.q  # вычисляем alpha % q
        if e == 0:  # если получили 0
            e = 1  # исправляем на 1
        return e

    def form_digital_signature(self, d, M, P):
        """
        Метод для формирования подписи из сообщения
        :param d: ключ подписи человека
        :param M: сообщение необходимое подписать
        :param P: точка на кривой, порядка q
        :return:
        """
        h = self._get_hash_(M)  # получаем хеш
        e = self._get_e_from_hash_(h)  # получаем e из хеша

        while True:
            while True:
                k = secrets.randbelow(self.elliptic_curve.q)  # берём случайное k
                r = self.elliptic_curve.multiply_point_by_number(P, k).x % self.elliptic_curve.q  # счиаем kP (mod q)
                if r != 0:  # если получили 0, то повторяем, иначе - выходим из цикла
                    break

            s = (r * d + k * e) % self.elliptic_curve.q  # считаем S
            if s != 0:  # если получили 0, то повторяем, иначе - выходим из цикла
                break

        sign = self.DigitalSignature(r, s, self.length)  # создаём временный объект подписи

        return sign.get_hex_signature()  # возвращаем 16чное представление подписи

    def check_digital_signature(self, person, M, sign):
        """
        Метод для проверки подписи
        :param person: человек, который подписывал
        :param M: сообщение, которое необходимо было подписать
        :param sign: подпись
        :return:
        """
        if not 0 < sign.r < self.elliptic_curve.q or not 0 < sign.s < self.elliptic_curve.q:  # проверяем 0 < r, s < q
            return False  # если не удоволетворяет, то подпись неверна

        h = self._get_hash_(M)  # вычисляем хеш сообщения
        e = self._get_e_from_hash_(h)  # вычисляем e

        v = pow(e, self.elliptic_curve.q - 2, self.elliptic_curve.q)  # вычисляем v = e^-1 (mod q)
        z1 = (sign.s * v) % self.elliptic_curve.q  # вычисляем z1 = s*v (mod q)
        z2 = -sign.r * v % self.elliptic_curve.q  # вычисляем z2 = -r*v (mod q)

        temp1 = self.elliptic_curve.multiply_point_by_number(person.P, z1)  # вычисляем z1*P
        temp2 = self.elliptic_curve.multiply_point_by_number(person.Q, z2)  # вычисляем z2*Q
        C = self.elliptic_curve.add_points(temp1, temp2)  # вычисляем z1*P + z2*Q
        R = C.x % self.elliptic_curve.q  # получаем Xc % q

        return R == sign.r  # если совпадает, то подпись верна

    @staticmethod
    def control_digital_signature_formation(length):
        """
        Метод для контрольной проверки формирования подписи
        :param length: длина p в битах
        :return:
        """
        curve = EllipticCurve.get_control_curve(length)  # берём контрольную кривую
        # в зависимости от длины p формируем нужные параметры
        if length == 256:
            point = Point(2, 4018974056539037503335449422937059775635739389905545080690979365213431566280)
            d = 55441196065363246126355624130324183196576709222340016572108097750006097525544
            e = 20798893674476452017134061561508270130637142515379653289952617252661468872421
            k = 53854137677348463731403841147996619241504003434302020712960838528893196233395
        else:
            point = Point(1928356944067022849399309401243137598997786635459507974357075491307766592685835441065557681003184874819658004903212332884252335830250729527632383493573274,
                          2288728693371972859970012155529478416353562327329506180314497425931102860301572814141997072271708807066593850650334152381857347798885864807605098724013854)
            d = 610081804136373098219538153239847583006845519069531562982388135354890606301782255383608393423372379057665527595116827307025046458837440766121180466875860
            e = 2897963881682868575562827278553865049173745197871825199562947419041388950970536661109553499954248733088719748844538964641281654463513296973827706272045964
            k = 175516356025850499540628279921125280333451031747737791650208144243182057075034446102986750962508909227235866126872473516807810541747529710309879958632945

        r = curve.multiply_point_by_number(point, k).x % curve.q  # вычисляем r
        s = (r * d + k * e) % curve.q  # вычисляем s

        return s

    @staticmethod
    def control_digital_signature_check(length):
        """
        Метод для контрольной проверки формирования подписи
        :param length: длина p в битах
        :return:
        """
        curve = EllipticCurve.get_control_curve(length)  # берём контрольную кривую

        # в зависимости от длины p формируем нужные параметры
        if length == 256:
            r = 29700980915817952874371204983938256990422752107994319651632687982059210933395
            s = 574973400270084654178925310019147038455227042649098563933718999175515839552
            e = 20798893674476452017134061561508270130637142515379653289952617252661468872421
            P = Point(2, 4018974056539037503335449422937059775635739389905545080690979365213431566280)
            Q = Point(57520216126176808443631405023338071176630104906313632182896741342206604859403,
                      17614944419213781543809391949654080031942662045363639260709847859438286763994)
        else:
            r = 2489204477031349265072864643032147753667451319282131444027498637357611092810221795101871412928823716805959828708330284243653453085322004442442534151761462
            s = 864523221707669519038849297382936917075023735848431579919598799313385180564748877195639672460179421760770893278030956807690115822709903853682831835159370
            P = Point(1928356944067022849399309401243137598997786635459507974357075491307766592685835441065557681003184874819658004903212332884252335830250729527632383493573274,
                      2288728693371972859970012155529478416353562327329506180314497425931102860301572814141997072271708807066593850650334152381857347798885864807605098724013854)
            Q = Point(909546853002536596556690768669830310006929272546556281596372965370312498563182320436892870052842808608262832456858223580713780290717986855863433431150561,
                      2921457203374425620632449734248415455640700823559488705164895837509539134297327397380287741428246088626609329139441895016863758984106326600572476822372076)
            e = 2897963881682868575562827278553865049173745197871825199562947419041388950970536661109553499954248733088719748844538964641281654463513296973827706272045964

        # вычисляем верна ли подпись
        v = pow(e, curve.q - 2, curve.q)
        z1 = (s * v) % curve.q
        z2 = -r * v % curve.q

        temp1 = curve.multiply_point_by_number(P, z1)
        temp2 = curve.multiply_point_by_number(Q, z2)
        C = curve.add_points(temp1, temp2)
        R = C.x % curve.q

        return R == r

    def get_appropriate_p_point(self):
        while True:
            try:
                return self.elliptic_curve.get_point_with_q_rank()
            except EllipticCurve.PPointException:
                a = self.elliptic_curve.a
                b = self.elliptic_curve.b
                self.elliptic_curve = EllipticCurve(a, b, self.length)


class Person:
    """
    Класс представления человека
    """
    def _get_d_(self):
        """
        Метод для получения d
        :return:
        """
        d = secrets.randbelow(self.curve.q)  # генерируем случайное число
        while d == 0:  # если получили 0 - перегенерируем
            d = secrets.randbelow(self.curve.q)
        return d

    def __init__(self, curve, point):
        """
        Конструктор
        :param curve: эллиптическая кривая
        :param point: точка на кривой порядка q
        """
        self.curve = curve
        self.d = self._get_d_()  # получаем d
        self.P = point
        self.Q = curve.multiply_point_by_number(self.P, self.d)  # получаем Q = dP


if __name__ == '__main__':
    def non_gost_flow(flow_type):
        if flow_type == 2:
            length = int(input('Введите размер в битах (<= 20): '))
            a = secrets.randbits(length)  # генерируем случайный коэффициент а
            b = secrets.randbits(length)  # генерируем случайный коэффициент b
            a |= 2 ** (length - 1)
            b |= 2 ** (length - 1)
        else:
            # словарь, который преобразует выбор пользователя (1-7) в размерность кривой
            choices = {
                1: 160,
                2: 192,
                3: 224,
                4: 256,
                5: 320,
                6: 384,
                7: 512,
            }
            print('Выберете кривую')
            print(f'1. 160-битная кривая' + '\n' +
                  f'2. 192-битная кривая' + '\n' +
                  f'3. 224-битная кривая' + '\n' +
                  f'4. 256-битная кривая' + '\n' +
                  f'5. 320-битная кривая' + '\n' +
                  f'6. 384-битная кривая' + '\n' +
                  f'7. 512-битная кривая')  # выводим информацию на консоль

            length = choices[int(input())]
            a = 1
            b = 1

        data = input('Введите сообщение, которое необходимо подписать: ')  # сообщение, которое подписываем
        gost = GOST(a, b, length, 'C:/Users/dXAH/source/repos/Stribog/Stribog/bin/Debug/Stribog.exe')  # создаём объект госта

        print(f'Работаем с кривой {gost.elliptic_curve}')
        pers = Person(gost.elliptic_curve, gost.get_appropriate_p_point())  # создаём человека

        # в случае, когда мы работаем со случайной кривой - выводим её параметры на консоль
        if flow_type == 2:
            print('\nПараметры кривой')
            print(f'Порядок группы кривой: {pers.curve.m}')
            print(f'Порядок выбранной цикличиской подгруппы: {pers.curve.q}')

        # всегда выводим информацию о ключах пользователя
        print()
        print(f'Выбранный ключ подписи: {pers.d}')
        print(f'Выбранный ключ проверки подписи:{pers.Q}')
        temp = gost.form_digital_signature(pers.d, data, pers.P)  # формируем подпись
        signature = GOST.DigitalSignature.from_hex_signature(temp)  # генерируем объект из подписи

        # проверка подписи всегда должна проходить
        print()
        print(f'Полученая подпись: {signature}')  # выводим подпись
        print(f'Подпись верна: {gost.check_digital_signature(pers, data, signature)}')  # проверяем подпись

        print()
        print(f'Меняем подпись')
        temp = list(temp)  # преобразуем строку к списку символов
        temp[random.randint(0, len(temp) - 1)] = 'f'  # меняем случайный символ на f этого недостаточно т.к. могли поменять f на f
        random.shuffle(temp)  # дополнительно перемешиваем строку
        temp = ''.join(temp)  # преобразуем обратнно к строке
        signature = GOST.DigitalSignature.from_hex_signature(temp)  # создаём новый объект

        # проверка изменённой подписи всегда должна НЕ проходить
        print(f'Изменённая подпись: {signature}')  # выводим изменённую подпись
        print(f'Подпись верна: {gost.check_digital_signature(pers, data, signature)}')  # проверяем её

    def gost_flow():
        # выводим возможности выбора
        print(f'1. 256 бит' + '\n' +
              f'2. 512 бит')
        length_choice = input()

        # в зависимости от выбранного размера кривой подготваливаем разные параметры длины и контрольного значения
        if length_choice == '1':
            length = 256
            check = GOST.LENGTH_256_CONTROL_S_VALUE
        else:
            length = 512
            check = GOST.LENGTH_512_CONTROL_S_VALUE

        res = GOST.control_digital_signature_formation(length)  # вычисляем контрольную подпись
        print(f'S = {res}' + '\n' +
              f'Совпадает с указанным в ГОСТе: {res == check}')  # выводим на экран подпись (S) и результат совпадения того, что получили и указанного в ГОСТе

        res2 = GOST.control_digital_signature_check(length)  # вычисляем результат проверки подписи
        print(f'Результат проверки подписи в ГОСТе: {res2}')  # выводим что получили

    print(f'1. Контрольный пример' + '\n' +
          f'2. Случайная кривая' + '\n' +
          f'3. Кривая из стандартов' + '\n' +
          f'4. Выход')  # выводим информацию на консоль

    choice = input()  # считываем выбор
    if choice == '1':
        # вызываем функцию для демонстрации работы
        gost_flow()
    elif choice == '2':
        # вызываем функцию для демонстрации работы
        non_gost_flow(2)
    elif choice == '3':
        # вызываем функцию для демонстрации работы
        non_gost_flow(3)
    else:
        exit(0)
